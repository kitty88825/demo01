import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BmiService } from '../bmi.service';

@Component({
  selector: 'app-lesson1',
  templateUrl: './lesson1.component.html',
  styleUrls: ['./lesson1.component.scss'],
  providers: [BmiService]  // 屬於自己的BmiService
})
export class Lesson1Component implements OnInit {
  tw = 1;
  @Output() twChange: EventEmitter<number> = new EventEmitter<number>();
  constructor(
    public bmiService: BmiService
  ) { }

  ngOnInit() {
  }

  moneyChange1(money: number) {
    this.twChange.emit(money);
  }

  callService() {
    this.bmiService.sayHello();
  }

  changeService() {
    this.bmiService.name = 'hhhh';
  }

}
