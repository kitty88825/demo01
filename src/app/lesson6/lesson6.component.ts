import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-lesson6',
  templateUrl: './lesson6.component.html',
  styleUrls: ['./lesson6.component.scss']
})
export class Lesson6Component implements OnInit {
  lesson6val: number;
  constructor() { }

  ngOnInit() {
  }

  mChange6(money: number) {
    this.lesson6val = money;
  }
}
