import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lesson3',
  templateUrl: './lesson3.component.html',
  styleUrls: ['./lesson3.component.scss']
})
export class Lesson3Component implements OnInit {
  weeks = ['星期一', '星期二', '星期三'];
  users = [
    {name: 'Lux', age: 18},
    {name: 'cuteKitty', age: 18}
  ];

  sroces = [10, 20, 30, 40, 50, 60, 70, 80];
  now = new Date();

  constructor() { }

  ngOnInit() {
  }

}
