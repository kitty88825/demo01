import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Lesson1Component } from './lesson1/lesson1.component';
import { Lesson2Component } from './lesson2/lesson2.component';
import { Lesson3Component } from './lesson3/lesson3.component';
import { Lesson4Component } from './lesson4/lesson4.component';
import { Lesson5Component } from './lesson5/lesson5.component';
import { Lesson6Component } from './lesson6/lesson6.component';
import { Lesson8Component } from './lesson8/lesson8.component';
import { Lesson9Component } from './lesson9/lesson9.component';


const routes: Routes = [
  { path: '', redirectTo: 'Lesson1', pathMatch: 'full' },
  { path: 'Lesson1', component: Lesson1Component },
  { path: 'Lesson2', component: Lesson2Component },
  { path: 'Lesson3', component: Lesson3Component },
  { path: 'Lesson4', component: Lesson4Component },
  { path: 'Lesson5', component: Lesson5Component },
  { path: 'Lesson6', component: Lesson6Component },
  { path: 'Lesson8', component: Lesson8Component },
  { path: 'Lesson9/:id', component: Lesson9Component },
  { path: '**', component: Lesson1Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
