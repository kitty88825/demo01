import { Component } from '@angular/core';
import { Account } from './model/account.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html', // 畫面在哪個檔案
  styleUrls: ['./app.component.scss'] // css 用哪個檔案
})
export class AppComponent {
  title = 'demo01';
  acc: Account = {
    account: 'user01',
    password: 'pwd'
  };

  url = 'http://www.google.com';
  age = 10;
  isShow = false;

  sayHi(word: string = 'Hello') {
    alert(word);
  }
}
