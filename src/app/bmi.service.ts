import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BmiService {
  name = 'Kitty';
  constructor() { }

  sayHello() {
    alert(this.name);
  }
}
